#define NUM_FLAG_BITS 1
#define LED_STATE 0


ATOMIC_DEFINE(flag_bits, NUM_FLAG_BITS);

bool set_flag_bit(int bit_position) {
  // returns previous value
  atomic_set_bit(flag_bits, bit_position);
  return true;
}

bool clear_flag_bit(int bit_position) {
  // returns previous value
  atomic_clear_bit(flag_bits, bit_position);
  return false;
}

bool test_flag_bit(int bit_position) {
  return atomic_test_bit(flag_bits, bit_position);
}

bool toggle_flag_bit(int bit_position) {
  if (test_flag_bit(bit_position)) {
    return clear_flag_bit(bit_position);
  } else {
    return set_flag_bit(bit_position);
  }
}


