struct led_dev {
  const struct device *dev;
  char *label;
  gpio_pin_t pin;
  gpio_flags_t flags;
};

struct led_dev leds[] = {
#define LED0_NODE DT_ALIAS(led0)
#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
    {
        .label = DT_GPIO_LABEL(LED0_NODE, gpios),
        .pin = DT_GPIO_PIN(LED0_NODE, gpios),
        .flags = DT_GPIO_FLAGS(LED0_NODE, gpios),
    },
#endif
#define LED1_NODE DT_ALIAS(led1)
#if DT_NODE_HAS_STATUS(LED1_NODE, okay)
    {
        .label = DT_GPIO_LABEL(LED1_NODE, gpios),
        .pin = DT_GPIO_PIN(LED1_NODE, gpios),
        .flags = DT_GPIO_FLAGS(LED1_NODE, gpios),
    },
#endif
#define LED2_NODE DT_ALIAS(led2)
#if DT_NODE_HAS_STATUS(LED2_NODE, okay)
    {
        .label = DT_GPIO_LABEL(LED2_NODE, gpios),
        .pin = DT_GPIO_PIN(LED2_NODE, gpios),
        .flags = DT_GPIO_FLAGS(LED2_NODE, gpios),
    },
#endif
#define LED3_NODE DT_ALIAS(led3)
#if DT_NODE_HAS_STATUS(LED3_NODE, okay)
    {
        .label = DT_GPIO_LABEL(LED3_NODE, gpios),
        .pin = DT_GPIO_PIN(LED3_NODE, gpios),
        .flags = DT_GPIO_FLAGS(LED3_NODE, gpios),
    },
#endif
};

#if BOARD == BOARD_NRF52_PUCKJS
const uint8_t led0_index = 1; // no extra green LED, re-use green led
const uint8_t blue_index = 0;
const uint8_t green_index = 1;
const uint8_t red_index = 2;
#else
const uint8_t led0_index = 0;
const uint8_t red_index = 1;
const uint8_t green_index = 2;
const uint8_t blue_index = 3;
#endif

void r(bool on) {
  struct led_dev *l = &(leds[red_index]);
  gpio_pin_set(l->dev, l->pin, on ? 1 : 0);
}

void g(bool on) {
  struct led_dev *l = &(leds[green_index]);
  gpio_pin_set(l->dev, l->pin, on ? 1 : 0);
}

void b(bool on) {
  struct led_dev *l = &(leds[blue_index]);
  gpio_pin_set(l->dev, l->pin, on ? 1 : 0);
}

void led(bool on) {
  struct led_dev *l = &(leds[led0_index]);
  gpio_pin_set(l->dev, l->pin, on ? 1 : 0);
}

void rgb(bool red, bool green, bool blue) {
  r(red);
  g(green);
  b(blue);
}

void setup_leds() {
  int err;
  struct led_dev *l;
  for (size_t i = 0; i < ARRAY_SIZE(leds); i++) {
    l = &leds[i];
    l->dev = device_get_binding(l->label);
    if (l->dev == NULL) {
      printk("Could not bind %s\n", l->label);
      continue;
    }

    err = gpio_pin_configure(l->dev, l->pin, GPIO_OUTPUT_ACTIVE | l->flags);
    if (err) {
      printk("Count not configure %s\n", l->label);
      continue;
    }
  }

  rgb(false, false, false);
}

