#include <drivers/uart.h>
#include <errno.h>
#include <kernel.h>
#include <stddef.h>
#include <string.h>
#include <sys/printk.h>
#include <zephyr.h>
#include <zephyr/types.h>
#include <sys/byteorder.h>
#include <device.h>
#include <devicetree.h>
#include <drivers/gpio.h>
#include <inttypes.h>
#include <sys/printk.h>
#include <sys/util.h>

#include <bluetooth/att.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/conn.h>
#include <bluetooth/gatt.h>
#include <bluetooth/hci.h>
#include <bluetooth/uuid.h>

#define NRFX_LOG_MODULE APP
#include <devicetree.h>
#include <nrfx_log.h>
#include <nrfx_nfct.h>
#include <nrfx_timer.h>

#include "buttons.h"
#include "leds.h"
#include "flags.h"

#define NFCT_BUFFER_SIZE 256

#define SLOW_UPDATE K_MSEC(500)

// 'system'
#define S_BLOCK_MASK 0b11000010
// 'reply'
#define R_BLOCK_MASK 0b10000010
// 'info'
#define I_BLOCK_MASK 0b00000010

#define BLOCK_NUM_MASK 0b00000001
//NAD: node address
#define NAD_MASK 0b00000100
//CID: card id
#define CID_MASK 0b00001000
#define CHAINING_MASK 0b00010000

//Forward declarations
static void nfct_event_handler(nrfx_nfct_evt_t const *p_event);

enum NFC_States {
  START,
  SELECTED,
  ACTIVATED,
};

enum NFC_States nfc_state = START;

// Declare constants
const int MIN_PRESS_TIME = 50;
const int SHORT_PRESS_TIME = 500;

static const nrfx_nfct_config_t nfct_config = {
    .rxtx_int_mask = 0xFFFFFFFF,
    .cb = nfct_event_handler,
};

static uint8_t rxframe[NFCT_BUFFER_SIZE] = {0};
static uint8_t txframe[NFCT_BUFFER_SIZE] = {0};
static bool rx_pending = false;

static bool m_nfct_activated = false;

static struct k_delayed_work led_work;


static void manage_led(struct k_work *work) {
  bool state = test_flag_bit(LED_STATE);
  led(state);
  k_delayed_work_submit(&led_work, SLOW_UPDATE);
}

void nfct_reset() {
  memset(rxframe, 0, NFCT_BUFFER_SIZE);
  memset(txframe, 0, NFCT_BUFFER_SIZE);
  rx_pending = false;
  m_nfct_activated = false;
  nfc_state = START;
}

void frame_rx_start() {
  if (rx_pending) {
    printk("RX: in progress...\r\n");
    return;
  }

  memset(rxframe, 0, NFCT_BUFFER_SIZE);
  rx_pending = true;

  nrfx_nfct_data_desc_t receive = {
      .data_size = NFCT_BUFFER_SIZE,
      .p_data = rxframe,
  };

  nrfx_nfct_rx(&receive);
}

void send14a(struct net_buf_simple msg) {
  NRFX_LOG_HEXDUMP_DEBUG(msg.data, msg.len);

  uint8_t data[NFCT_BUFFER_SIZE] = {0};
  size_t size = msg.len + 1;
  if (size > NFCT_BUFFER_SIZE) {
    printk("TODO: chain\n");
    return;
  }
  data[0] = I_BLOCK_MASK;
  memcpy(data+1, msg.data, msg.len);

  NRFX_LOG_HEXDUMP_DEBUG(data, size);
  send(data, size);
}

void send(const uint8_t* data, size_t size) {
  memset(txframe, 0, NFCT_BUFFER_SIZE);
  memcpy(txframe, data, size);

  nrfx_nfct_data_desc_t transmit = {
      .data_size = size,
      .p_data = txframe,
  };
  nrfx_nfct_tx(&transmit, NRF_NFCT_FRAME_DELAY_MODE_WINDOW);
}

void parseAPDU(struct net_buf_simple msg) {
  const uint8_t fileNotFound[] = {0x6A, 0x82};
  const uint8_t success[] = {0x90, 0x00};

  NRFX_LOG_HEXDUMP_DEBUG(msg.data, msg.len);

  struct net_buf_simple response = {
    .data = fileNotFound,
    .len = sizeof(fileNotFound),
    .size = sizeof(fileNotFound),
  };

  send14a(response);
}

// http://www.emutag.com/iso/14443-4.pdf 7.1.1.1 (pg15)
void parse14a(struct net_buf_simple msg) {
  size_t i = 0;
  if (msg.data[i] & S_BLOCK_MASK == S_BLOCK_MASK) {
    printk("S block\n");
    i++;
  } else if (msg.data[i] & R_BLOCK_MASK == R_BLOCK_MASK) {
    printk("R block\n");
    i++;
  } else { // Implicitly I
    printk("I block\n");
    uint8_t blockNum = msg.data[i] & BLOCK_NUM_MASK;
    bool nad = msg.data[i] & NAD_MASK;
    bool cid = msg.data[i] & CID_MASK;
    bool chain = msg.data[i] & CHAINING_MASK;
    i++;
    if (nad) {
      i++;
    }
    if (cid) {
      i++;
    }
    if (chain) {
      printk("TODO: chain\n");
    } else {
      struct net_buf_simple apdu = {
        .data = msg.data + i,
        .len = msg.len - i,
        .size = msg.size - i,
      };

      parseAPDU(apdu);
    }
  }
}

void frame_rx_end(const nrfx_nfct_evt_rx_frameend_t *rx_frameend) {
  uint32_t status = rx_frameend->rx_status;
  const uint8_t *data = rx_frameend->rx_data.p_data;
  size_t size = rx_frameend->rx_data.data_size;

  if (status) {
    if ((status & NRF_NFCT_RX_FRAME_STATUS_CRC_MASK) == NRF_NFCT_RX_FRAME_STATUS_CRC_MASK) {
      printk("rx end status CRC error\n");
    }
    if ((status & NRF_NFCT_RX_FRAME_STATUS_PARITY_MASK) == NRF_NFCT_RX_FRAME_STATUS_PARITY_MASK) {
      printk("rx end status PARITY error\n");
    }
    if ((status & NRF_NFCT_RX_FRAME_STATUS_OVERRUN_MASK) == NRF_NFCT_RX_FRAME_STATUS_OVERRUN_MASK) {
      printk("rx end status OVERRUN error\n");
    }
    rx_pending = false;
    return;
  }

  if (!rx_pending) {
    printk("RX: %d bytes frame dropped: \n", size);
    NRFX_LOG_HEXDUMP_DEBUG(data, size);
    return;
  }

  rx_pending = false;

  if (size == 0) {
    printk("RX: empty frame ?\n");
    return;
  }

  //printk("RX: command: \n");
  NRFX_LOG_HEXDUMP_DEBUG(data, size);

  //Curiously, with the PM3, I didn't see the select_uid come into this function
  const uint8_t SELECT_UID[] = {0x93, 0x70};
  const uint8_t SAK[] = {0x20};
  const uint8_t RATS[] = {0xe0};
  const uint8_t ATS[] = {0x06, 0x77, 0x77, 0x61, 0x02, 0x80};
  const uint8_t ISODEP[] = {0xD0, 0x11, 0x00};
  const uint8_t PPSS[] = {0xd0};
  //0a003c00 //mfp info
  //029060000000 //mfdes info
  //D01100 //ISO-DEP

  //https://www.nxp.com/docs/en/application-note/AN12196.pdf
  // Someone check me on my nfc_state names and use, i'm not confident
  if (nfc_state == START && memcmp(data, SELECT_UID, sizeof(SELECT_UID)) == 0) {
    send(SAK, sizeof(SAK));
    nfc_state = SELECTED;
  } else if ((nfc_state == START || nfc_state == SELECTED) && memcmp(data, RATS, sizeof(RATS)) == 0) {
    send(ATS, sizeof(ATS));
    nfc_state = ACTIVATED;
    printk("RATS second byte %02x\n", data[1]);
    //TODO: Parse second byte and act appropriately
  } else if (nfc_state == ACTIVATED && memcmp(data, ISODEP, size) == 0) {
    send(PPSS, sizeof(PPSS));
    // TODO: New nfc_state for this
  } else if (nfc_state == ACTIVATED) {
    const struct net_buf_simple msg = {
      .data = data,
      .size = size,
      .len = size,
    };
    parse14a(msg);
  } else {
    printk("unhandled frame\n");
  }
}

static void nfct_event_handler(nrfx_nfct_evt_t const *p_event) {
  switch (p_event->evt_id) {
  case NRFX_NFCT_EVT_FIELD_DETECTED:
    if (!m_nfct_activated) {
      nrfx_nfct_state_force(NRFX_NFCT_STATE_ACTIVATED);
      m_nfct_activated = true;
    }
    break;
  case NRFX_NFCT_EVT_SELECTED:
    frame_rx_start();
    break;
  case NRFX_NFCT_EVT_RX_FRAMESTART:
    break;
  case NRFX_NFCT_EVT_RX_FRAMEEND:
    frame_rx_end(&p_event->params.rx_frameend);
    break;
  case NRFX_NFCT_EVT_TX_FRAMESTART:
    break;
  case NRFX_NFCT_EVT_TX_FRAMEEND:
    frame_rx_start();
    break;
  case NRFX_NFCT_EVT_FIELD_LOST:
    m_nfct_activated = false;
    break;
  case NRFX_NFCT_EVT_ERROR:
    if (p_event->params.error.reason == NRFX_NFCT_ERROR_FRAMEDELAYTIMEOUT) {
      printk("FRAME DELAY TIMEOUT\n");
      nfct_reset();
    }
    break;
  default:
    printk("NFCT event %d\n", p_event->evt_id);
    break;
  }
}

void main(void) {
  setup_leds();

  int err = bt_enable(NULL);
  if (err) {
    printk("err enabling bt %d\n", err);
  }

  k_delayed_work_init(&led_work, manage_led);
  k_delayed_work_submit(&led_work, K_MSEC(0));


  IRQ_CONNECT(NFCT_IRQn, NRFX_NFCT_DEFAULT_CONFIG_IRQ_PRIORITY, nrfx_isr, nrfx_nfct_irq_handler, 0);
  IRQ_CONNECT(TIMER4_IRQn, NRFX_NFCT_DEFAULT_CONFIG_IRQ_PRIORITY, nrfx_isr, nrfx_timer_4_irq_handler, 0);

  nrfx_err_t nfcErr = nrfx_nfct_init(&nfct_config);
  if (nfcErr == NRFX_SUCCESS) {
    printk("Initialized NFCT\n");
  } else if (nfcErr == NRFX_ERROR_INVALID_STATE) {
    printk("NFCT: Invalid State\n");
  } else {
    printk("Error %d with nrfx_nfct_init\n", nfcErr);
  }

  static uint8_t const p_id[NRFX_NFCT_NFCID1_SINGLE_SIZE] = {0xff, 0xff, 0xff, 0xff};
  static const nrfx_nfct_nfcid1_t nfcid1 = {
      .id_size = NRFX_NFCT_NFCID1_SINGLE_SIZE,
      .p_id = p_id,
  };

  static const nrfx_nfct_param_t nfcid_param = {
      .id = NRFX_NFCT_PARAM_ID_NFCID1,
      .data.nfcid1 = nfcid1,
  };

  nfcErr = nrfx_nfct_parameter_set(&nfcid_param);
  if (nfcErr != NRFX_SUCCESS) {
    printk("Failed to set nfcid\n");
  }

  static const nrfx_nfct_param_t t4t_param = {
      .id = NRFX_NFCT_PARAM_ID_SEL_RES,
      .data.sel_res_protocol = NRF_NFCT_SELRES_PROTOCOL_T4AT,
      //.data.sel_res_protocol = NRF_NFCT_SELRES_PROTOCOL_NFCDEP_T4AT,
  };
  nfcErr = nrfx_nfct_parameter_set(&t4t_param);
  if (nfcErr != NRFX_SUCCESS) {
    printk("Failed to set t4t\n");
  }

  nrfx_nfct_enable();
}
